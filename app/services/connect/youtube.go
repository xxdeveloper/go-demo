package connect

import (
	"flag"
	"google.golang.org/api/googleapi/transport"
	"google.golang.org/api/youtube/v3"
	"log"
	"net/http"
)

const youtubeKey = "AIzaSyCLD38olvI31DmMMwcvAidz2K6cHIDjWi0"

var (
	YService   YoutubeService
	queryTrailers  = flag.String("query1", "Трейлер фильма", "Search trailers")
	queryEducation = flag.String("query2", "Уроки по React", "Search lessons")
	maxResults = flag.Int64("max-results", 25, "Max YouTube results")
)

func init() {
	client := &http.Client{
		Transport: &transport.APIKey{Key: youtubeKey},
	}

	service, err := youtube.New(client)
	if err != nil {
		log.Fatalf("Error creating new YouTube client: %v", err)
	}

	YService = YoutubeService{client: *client, service: service}
}

// YoutubeService module
type YoutubeService struct {
	client  http.Client
	service *youtube.Service
}

// TwitchFeed is array of top twitch videos
type YoutubeFeed struct {
	TopStreams []TwitchItem `json:"streams"` // For test and fast release
}

func (svc *YoutubeService) GetTop() *YoutubeFeed {

	streams := YoutubeFeed{}
	call := svc.service.Videos.List("id, snippet,statistics").MaxResults(*maxResults)
	call = call.Chart("mostPopular")
	call = call.RegionCode("RU")
	call = call.VideoCategoryId("")

	response, err := call.Do()
	if err != nil {
		log.Fatalf("Error making search API call: %v", err)
	}

	for _, item := range response.Items {
		video := TwitchItem{}

		channel := TwitchChannel{}
		channel.URL = item.Id
		channel.Name = item.Snippet.ChannelTitle

		video.Name = item.Snippet.Title
		video.Viewers = uint32(item.Statistics.ViewCount)

		video.Images = *fetchImagesInfo(item.Snippet.Thumbnails)
		video.Channel = channel

		streams.TopStreams = append(streams.TopStreams, video)
	}

	return &streams
}

// Get Videos by category
func (svc *YoutubeService) SearchVideos(t string) *YoutubeFeed {

	streams := YoutubeFeed{}
	query := selectQuery(t)
	call := svc.service.Search.List("id,snippet").Q(*query).MaxResults(*maxResults)

	response, err := call.Do()
	if err != nil {
		log.Fatalf("Error making search API call: %v", err)
	}

	// Iterate through each item and add it to the correct list
	for _, item := range response.Items {
		switch item.Id.Kind {
		case "youtube#video":
			video := fetchVideoInfo(item)
			streams.TopStreams = append(streams.TopStreams, *video)
		}
	}

	return &streams
}

func (svc *YoutubeService) GetMusic() *YoutubeFeed {

	streams := YoutubeFeed{}
	call := svc.service.Search.List("id,snippet").ChannelId("UCulYu1HEIa7f70L2lYZWHOw").MaxResults(*maxResults)

	response, err := call.Do()
	if err != nil {
		log.Fatalf("Error making search API call: %v", err)
	}

	// Iterate through each item and add it to the correct list
	for _, item := range response.Items {
		switch item.Id.Kind {
		case "youtube#video":
			video := fetchVideoInfo(item)
			streams.TopStreams = append(streams.TopStreams, *video)
		}
	}

	return &streams
}

func selectQuery(t string) *string {
	result := queryTrailers
	if t == "education" {
		result = queryEducation
	};

	return result;
}

func fetchVideoInfo(item *youtube.SearchResult) *TwitchItem {

	video := TwitchItem{}
	channel := TwitchChannel{}
	channel.URL = item.Id.VideoId
	channel.Name = item.Snippet.ChannelTitle

	video.Name = item.Snippet.Title
	video.Images = *fetchImagesInfo(item.Snippet.Thumbnails)
	video.Channel = channel

	return &video
}

func fetchImagesInfo(item *youtube.ThumbnailDetails) *ImagesBlock {
	img := ImagesBlock{}
	img.Small = item.Default.Url
	img.Medium = item.Medium.Url
	img.Large = item.High.Url

	return &img
}
