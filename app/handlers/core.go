package handlers

import (
	"net/http"
	"io"
	"errors"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

// Health check
func CoreRoutes() chi.Router {
	router := chi.NewRouter()

	router.Get("/health", HealthCheckHandler)
	router.Get("/json", JsonHandler)

	return router
}

func HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")

	// In the future we could report back on the status of our DB, or our cache
	// (e.g. Redis) by performing a simple PING, and include them in the response.
	io.WriteString(w, `{"alive": true}`)
}

func JsonHandler(w http.ResponseWriter, r *http.Request) {
	render.Render(w, r, ErrNotFound(errors.New("Test")))
}
