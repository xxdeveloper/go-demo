package connect

import (
	"github.com/xxdeveloper/go-oembed/oembed"
	"io/ioutil"
	"bytes"
	"log"
	"github.com/spf13/viper"
)

func NewOembedService(provider string, cfg *viper.Viper) (*OEmbedService, error) {

	source := cfg.GetString("config")
	data, err := ioutil.ReadFile(source)
	if err != nil {
		return nil, err
	}
	srv := &OEmbedService{oe: oembed.NewOembed(), source: source}

	if err = srv.oe.ParseProviders(bytes.NewReader(data)); err != nil {
		return nil, err
	}

	return srv, nil
}

type OEInfo struct {
	Status          int    `json:"-"`
	Type            string `json:"type,omitempty"`
	CacheAge        uint64 `json:"cache_age,omitempty"`
	URL             string `json:"url,omitempty"`
	ProviderURL     string `json:"provider_url,omitempty"`
	ProviderName    string `json:"provider_name,omitempty"`
	Title           string `json:"title,omitempty"`
	Description     string `json:"description,omitempty"`
	Width           uint64 `json:"width,omitempty"`
	Height          uint64 `json:"height,omitempty"`
	ThumbnailURL    string `json:"thumbnail_url,omitempty"`
	ThumbnailWidth  uint64 `json:"thumbnail_width,omitempty"`
	ThumbnailHeight uint64 `json:"thumbnail_height,omitempty"`
	AuthorName      string `json:"author_name,omitempty"`
	AuthorURL       string `json:"author_url,omitempty"`
	HTML            string `json:"html,omitempty"`
}

type OEmbedService struct {
	source string
	oe     *oembed.Oembed
}

func (srv *OEmbedService) GetDataFrom(url string) (*OEInfo, error) {

	item := srv.oe.FindItem(url)

	if item != nil {
		info, err := item.FetchOembed(oembed.Options{URL: url })
		if err != nil {
			log.Println("An error occured: %s\n", err.Error())
		} else {
			if info.Status >= 300 {
				log.Println("Response status code is: %d\n", info.Status)
			} else {
				log.Println("Oembed info:\n%s\n", info)
			}
		}
		return srv.convertToOEInfo(info), nil
	} else {
		log.Println("nothing found :(")
		return nil, nil
	}

}

func (srv *OEmbedService) convertToOEInfo(info *oembed.Info) *OEInfo {
	res := &OEInfo{
		Status:          info.Status,
		Type:            info.Type,
		URL:             info.URL,
		ProviderURL:     info.ProviderURL,
		ProviderName:    info.ProviderName,
		Title:           info.Title,
		Description:     info.Description,
		Width:           info.Width,
		Height:          info.Height,
		ThumbnailURL:    info.ThumbnailURL,
		ThumbnailWidth:  info.ThumbnailWidth,
		ThumbnailHeight: info.ThumbnailHeight,
		AuthorName:      info.AuthorName,
		AuthorURL:       info.AuthorURL,
		HTML:            info.HTML,
	}

	return res
}
