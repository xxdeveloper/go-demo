package handlers

import (
	"demo/app/services/cache"
	"demo/app/services/connect"
	"encoding/json"
	"github.com/go-chi/chi"
	"log"
	"net/http"
)

type Feed struct {
	Model  cache.Cache
	Prefix string
}

func (svc *Feed) Routes() chi.Router {
	router := chi.NewRouter()

	router.Get("/twitch-top", svc.twitchTopHandler)
	router.Get("/youtube-top", svc.YoutubeTopHandler)
	router.Get("/youtube-trailers", svc.YoutubeTrailersHandler)
	router.Get("/youtube-education", svc.YoutubeEducationHandler)
	router.Get("/youtube-music", svc.YoutubeMusicHandler)

	return router
}

func (svc *Feed) YoutubeTopHandler(w http.ResponseWriter, r *http.Request) {
	// Check the cache first
	data, err := svc.Model.Get("youtube_top")
	if err != nil {
		log.Println(err.Error())
		// Request data from Youtube API
		streams := connect.YService.GetTop()
		data, err = json.Marshal(streams)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Set Cache
		err = svc.Model.Set("youtube_top", data)
		if err != nil {
			log.Println(err.Error())
		}
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

func (svc *Feed) YoutubeTrailersHandler(w http.ResponseWriter, r *http.Request) {
	// Check the cache first
	data, err := svc.Model.Get("trailers")
	if err != nil {
		log.Println(err.Error())

		// Request data from Youtube API
		streams := connect.YService.SearchVideos("trailers")
		data, err = json.Marshal(streams)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Set Cache
		err = svc.Model.Set("trailers", data)
		if err != nil {
			log.Println("Set cache error")
			log.Println(err.Error())
		}
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

func (svc *Feed) YoutubeEducationHandler(w http.ResponseWriter, r *http.Request) {
	// Check the cache first.
	data, err := svc.Model.Get("education")
	if err != nil {
		log.Println(err.Error())

		// Request data from Youtube API
		streams := connect.YService.SearchVideos("education")
		data, err = json.Marshal(streams)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Set Cache
		err = svc.Model.Set("education", data)
		if err != nil {
			log.Println(err.Error())
		}
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

func (svc *Feed) YoutubeMusicHandler(w http.ResponseWriter, r *http.Request) {
	// Check the cache first.
	data, err := svc.Model.Get("music")
	if err != nil {
		log.Println(err.Error())

		// Request data from Youtube API
		streams := connect.YService.GetMusic()
		data, err = json.Marshal(streams)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Set Cache
		err = svc.Model.Set("music", data)
		if err != nil {
			log.Println(err.Error())
		}
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

func (svc *Feed) twitchTopHandler(w http.ResponseWriter, r *http.Request) {
	// Check the cache first.
	data, err := svc.Model.Get("twitch")
	if err != nil {
		log.Println(err.Error())

		// Request data from Twitch
		streams := connect.TService.GetTop()
		data, err = json.Marshal(streams)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Save to Cache
		err = svc.Model.Set("twitch", data)
		if err != nil {
			log.Println(err.Error())
		}
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}
