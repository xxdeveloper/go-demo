package cache

import (
	"time"
	"github.com/bradfitz/gomemcache/memcache"
	"errors"
	"github.com/spf13/viper"
	"log"
)

func init() {
	register("memcache", &MemcacheDriver{})
}

type MemcacheDriver struct{}

func (d *MemcacheDriver) OpenConnection(cfg *viper.Viper) (Cache, error) {

	connection := cfg.GetString("host") + ":" + cfg.GetString("port")
	log.Print(connection)
	connections := []string{connection}
	mc := memcache.New(connections...)
	mc.Timeout = 5 * time.Second

	if mc.FlushAll() != nil {
		return nil, errors.New("no servers configured or available")
	}

	service := &MemCache{
		cache:      mc,
		expiration: 300, // 5min
		prefix:     "test", // prefix
	}

	return service, nil
}

//  Memcached cache implementation
type MemCache struct {
	cache      *memcache.Client
	expiration time.Duration
	prefix     string
}

func (c *MemCache) Prefixed(str string) string {
	if c.prefix == "" {
		return str
	}
	return c.prefix + ":" + str
}

func (c *MemCache) Get(key string) ([]byte, error) {
	kk := c.Prefixed(key);
	item, err := c.cache.Get(kk)
	if err != nil {
		return nil, err
	}

	return item.Value, nil
}

func (c *MemCache) GetMany(keys []string) (map[string][]byte, error) {
	it, err := c.cache.GetMulti(keys)
	if err != nil {
		return nil, err
	}

	res := make(map[string][]byte)
	for _, v := range it {
		res[v.Key] = v.Value
	}
	return res, nil
}

func (c *MemCache) Set(key string, value []byte, expiration ...time.Duration) error {
	d := c.expiration
	if len(expiration) > 0 {
		d = expiration[0]
	}

	return c.cache.Set(&memcache.Item{
		Key:        c.Prefixed(key),
		Value:      value,
		Expiration: int32(d),
	})
}

func (c *MemCache) Delete(key string) error {
	return c.cache.Delete(key)
}

func (c *MemCache) Increment(key string, value ...int) (int, error) {
	n := 1
	if len(value) > 0 {
		n = value[0]
	}

	res, err := c.cache.Increment(c.Prefixed(key), uint64(n))

	return int(res), err
}

func (c *MemCache) Decrement(key string, value ...int) (int, error) {
	n := 1
	if len(value) > 0 {
		n = value[0]
	}

	res, err := c.cache.Decrement(c.Prefixed(key), uint64(n))

	return int(res), err
}

func (c *MemCache) Flush() error {
	return c.cache.FlushAll()
}

func (c *MemCache) Close() error {
	return nil
}
