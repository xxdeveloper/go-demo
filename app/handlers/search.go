package handlers

import (
	"demo/app/services/connect"
	"demo/app/validators"
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"net/http"
)

type Search struct {
	OEmbed *connect.OEmbedService
}

func (srv *Search) Routes() chi.Router {

	router := chi.NewRouter()
	router.Post("/fetch-url", srv.GetOEmbedData)

	return router
}

func (srv *Search) GetOEmbedData(w http.ResponseWriter, r *http.Request) {

	var err error
	data := &validators.UrlCredentials{}
	if err = json.NewDecoder(r.Body).Decode(data); err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	res, err := srv.OEmbed.GetDataFrom(data.Url)

	render.Respond(w, r, res)
}
