package main

import (
	"demo/app"
	"demo/config"
)

func main() {
	config.Get()

	app := &app.Root{}
	app.Initialize()
	defer app.Unitialize()

	// Start Server
	app.Run(":3333")
}
