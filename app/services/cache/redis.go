package cache

import (
	"time"
	"github.com/garyburd/redigo/redis"
	"github.com/spf13/viper"
)

const (
	REDIS_POOL_MAX_ACTIVE   = 20
	REDIS_POOL_MAX_IDLE     = 10
	REDIS_POOL_IDLE_TIMEOUT = 200 * time.Millisecond
	REDIS_EXPIRATION        = 60 * time.Second
	REDIS_PREFIX            = ""
)

func init() {
	register("redis", &RedisDriver{})
}

type RedisDriver struct{}

type PoolSettings struct {
	MaxIdle     int
	MaxActive   int
	IdleTimeout time.Duration
}

func (d *RedisDriver) OpenConnection(cfg *viper.Viper) (Cache, error) {
	//switch cfg.Get("connection").(type) {
	//case *redis.Pool:
	//	break
	//default:
	//	return nil, fmt.Errorf("Connection %s is unknown", cfg)
	//}
	pool := &redis.Pool{
		Dial: func() (redis.Conn, error) {
			return redis.DialURL("redis://"+cfg.GetString("database")+":"+cfg.GetString("password")+"@"+cfg.GetString("host")+":"+cfg.GetString("port"))
		},
	}

	service := &RedisCache{
		pool: pool,
		poolSettings: PoolSettings{
			MaxActive:   REDIS_POOL_MAX_ACTIVE,
			MaxIdle:     REDIS_POOL_MAX_IDLE,
			IdleTimeout: REDIS_POOL_IDLE_TIMEOUT * time.Millisecond,
		},
		expiration: REDIS_EXPIRATION,
		prefix:     REDIS_PREFIX,
	}

	return service, nil
}

// Redis cache implementation
type RedisCache struct {
	pool         *redis.Pool
	poolSettings PoolSettings
	expiration   time.Duration
	prefix       string
}

func (c *RedisCache) Prefixed(str string) string {
	if c.prefix == "" {
		return str
	}
	return c.prefix + ":" + str
}

func (c *RedisCache) Get(key string) ([]byte, error) {
	conn := c.pool.Get()
	defer conn.Close()

	if result, err := redis.Bytes(conn.Do("GET", c.Prefixed(key))); err != nil {
		return nil, err
	} else {
		return result, nil
	}
}

func (c *RedisCache) GetMany(keys []string) (map[string][]byte, error) {
	conn := c.pool.Get()
	defer conn.Close()

	it, err := redis.ByteSlices(conn.Do("MGET", keys));
	if err != nil {
		return nil, err
	}

	var res map[string][]byte
	for k, v := range it {
		res[string(k)] = v
	}

	return res, nil
}

func (c *RedisCache) Set(key string, value []byte, expiration ...time.Duration) error {
	conn := c.pool.Get()
	defer conn.Close()

	d := c.expiration
	if len(expiration) > 0 {
		d = expiration[0]
	}

	args := redis.Args{}.Add(c.Prefixed(key)).Add(value)
	ms := int(d / time.Millisecond)

	if ms > 0 {
		args = args.Add("PX").Add(ms)
	}

	_, err := conn.Do("SET", args...)
	return err
}

func (c *RedisCache) Delete(key string) error {
	conn := c.pool.Get()
	defer conn.Close()

	_, err := conn.Do("DEL", c.Prefixed(key))
	return err
}

func (c *RedisCache) Increment(key string, value ...int) (int, error) {
	conn := c.pool.Get()
	defer conn.Close()

	n := 1
	if len(value) > 0 {
		n = value[0]
	}

	return redis.Int(conn.Do("INCRBY", c.Prefixed(key), n))
}

func (c *RedisCache) Decrement(key string, value ...int) (int, error) {
	conn := c.pool.Get()
	defer conn.Close()

	n := 1
	if len(value) > 0 {
		n = value[0]
	}

	return redis.Int(conn.Do("DECRBY", c.Prefixed(key), n))
}

func (c *RedisCache) Flush() error {
	return nil
}

// closes cache (it's underlying connection)
func (c *RedisCache) Close() error {
	return c.pool.Close()
}
