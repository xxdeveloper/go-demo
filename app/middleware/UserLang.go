package middleware

import (
	"net/http"
	"context"
)

// UserLang is detect user language by request header
func UserLang(fallback string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			lang := r.Header.Get("Accept-Language")
			if lang == "" {
				lang = fallback
			}
			r = r.WithContext(context.WithValue(r.Context(), "api.lang", lang))
			next.ServeHTTP(w, r)
		})
	}
}
