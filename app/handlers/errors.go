package handlers

import (
	"net/http"
	"github.com/go-chi/render"
)

// ApiError renderer type for handling all sorts of errors.

type ApiError struct {
	Err        error  `json:"-"`               // low-level runtime error
	HttpStatus int    `json:"-"`               // http response status code
	Code       int    `json:"code,omitempty"`  // application-specific error code
	Message    string `json:"status"`          // user-level status message
	Debug      string `json:"error,omitempty"` // application-level error message, for debugging
}

func (e *ApiError) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HttpStatus)
	return nil
}

func (e *ApiError) Error() string { return e.Message }

func ErrInvalidRequest(err error) render.Renderer {
	return &ApiError{
		Err:        err,
		HttpStatus: http.StatusBadRequest,
		Code:       400,
		Message:    "Bad Request",
		Debug:      err.Error(),
	}
}

func ErrForbidden(err error) render.Renderer {
	return &ApiError{
		Err:        err,
		HttpStatus: http.StatusForbidden,
		Code:       403,
		Message:    "Forbidden",
		Debug:      err.Error(),
	}
}

func ErrUnauthorized(err error) render.Renderer {
	return &ApiError{
		Err:        err,
		HttpStatus: http.StatusUnauthorized,
		Code:       401,
		Message:    "Unauthorized",
		Debug:      err.Error(),
	}
}

func ErrInternalError(err error) render.Renderer {
	return &ApiError{
		Err:        err,
		HttpStatus: http.StatusInternalServerError,
		Code:       500,
		Message:    "Internal Server Error",
		Debug:      err.Error(),
	}
}

func ErrNotFound(err error) render.Renderer {
	return &ApiError{
		Err:        err,
		HttpStatus: http.StatusOK,
		Code:       404,
		Message:    "Not found",
		Debug:      err.Error(),
	}
}

func ErrDublicateError(err error) render.Renderer {
	return &ApiError{
		Err:        err,
		HttpStatus: http.StatusUnauthorized,
		Code:       401,
		Message:    "Duplicate",
		Debug:      err.Error(),
	}
}

func ErrRender(err error) render.Renderer {
	return &ApiError{
		Err:        err,
		HttpStatus: http.StatusUnprocessableEntity, // 422
		Message:    "Error rendering response",
		Debug:      err.Error(),
	}
}

var ErrNotImplemented = &ApiError{
	HttpStatus: http.StatusGone, // 410
	Code:       410,
	Message:    "This request not implemented yet",
}

var ErrLinkExpired = &ApiError{
	HttpStatus: http.StatusForbidden, // 403
	Code:       403,
	Message:    "Link expired",
}

var ErrLinkUsed = &ApiError{
	HttpStatus: http.StatusNotModified, // 304
	Code:       304,
	Message:    "Link already used",
}

var ErrMustVerify = &ApiError{
	HttpStatus: 231,
	Code:       231,
	Message:    "User must verify account",
}
