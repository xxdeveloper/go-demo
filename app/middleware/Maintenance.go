package middleware

import (
"net/http"
"context"
)

// UserLang is detect user language by request header
func Maintenance(mode bool) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if mode == true {
				// return maintenance response
			}
			r = r.WithContext(context.WithValue(r.Context(), "api.maintenance", mode))
			next.ServeHTTP(w, r)
		})
	}
}

