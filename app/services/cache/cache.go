package cache

import (
	"time"
	"errors"
	"github.com/spf13/viper"
)

var drivers = make(map[string]driver)

type driver interface {
	OpenConnection(cfg *viper.Viper) (Cache, error)
}

type Cache interface {
	Get(key string) ([]byte, error)
	GetMany(keys []string) (map[string][]byte, error)
	Set(key string, value []byte, expiration ...time.Duration) error
	//SetMany(array map[string][]byte, expiration ...time.Duration) error
	Increment(key string, value ...int) (int, error)
	Decrement(key string, value ...int) (int, error)
	// Forever(key string, value []byte)
	Delete(key string) error
	Flush() error
	// GetPrefix() string
	// Return cache to cache pool
	Close() error
}

func NewService(provider string, cfg *viper.Viper) (Cache, error) {
	driver, ok := drivers[provider]
	if !ok {
		return nil, errors.New("Provider not implemented yet: " + provider)
	}
	return driver.OpenConnection(cfg)
}

/*
 * Register makes a cache driver available by the provided name.
 * If Register is called twice with the same name or if driver is nil, it panics.
*/
func register(name string, drv driver) {
	if drv == nil {
		panic("Cache service: Registered driver is nil")
	}
	if _, dup := drivers[name]; dup {
		panic("Cache service: Register called twice for driver " + name)
	}
	drivers[name] = drv
}
