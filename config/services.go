package config

import (
	"demo/app/services/cache"
	"demo/app/services/connect"
	"errors"
	"github.com/spf13/viper"
	"log"
	"reflect"
	"sort"
	"strings"
)

var servicesList map[string]interface{} = map[string]interface{}{
	"cache":  cache.NewService,
	"oembed": connect.NewOembedService,
}

func InitService(name string) interface{} {
	var err error
	var result []reflect.Value
	if cfg, ok := getServiceConfig(name); ok {
		result, err = call(servicesList, name, cfg.Provider, cfg.Config)
		if err == nil {
			log.Println("(" + strings.ToUpper(name) + ") ... Initialized")
			return result[0].Interface()
		} else {
			log.Fatalln("(" + strings.ToUpper(name) + ") " + err.Error())
		}
	}
	return nil
}

func GetServicesList() []string {
	keys := make([]string, len(servicesList))

	i := 0
	for k := range servicesList {
		keys[i] = k
		i++
	}
	sort.Strings(keys)
	return keys
}

func GetAvailableServicesList() []string {
	source := viper.GetStringMap("settings")
	keys := []string{}

	for k, v := range source {
		if v.(map[string]interface{})["enabled"] == true {
			keys = append(keys, k)
		}
	}
	sort.Strings(keys)
	return keys
}

type serviceConfig struct {
	Provider string
	Config   *viper.Viper
}

func getServiceConfig(service string) (*serviceConfig, bool) {
	if viper.GetBool("settings." + service + ".enabled") {
		cfg := &serviceConfig{}
		cfg.Provider = viper.GetString("settings." + service + ".provider")
		if cfg.Provider == "" {
			cfg.Config = viper.Sub("services." + service)
		} else {
			cfg.Config = viper.Sub("services." + service + "." + cfg.Provider)
		}
		return cfg, true
	}
	return nil, false
}

func assert(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func call(m map[string]interface{}, name string, params ...interface{}) (result []reflect.Value, err error) {
	f := reflect.ValueOf(m[name])
	if len(params) != f.Type().NumIn() {
		err = errors.New("Configuration: The number of params is not adapted.")
		return
	}
	in := make([]reflect.Value, len(params))
	for k, param := range params {
		in[k] = reflect.ValueOf(param)
	}
	result = f.Call(in)
	if result[1].Interface() != nil {
		err = result[1].Interface().(error)
	}
	return
}
