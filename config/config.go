package config

import (
	"github.com/spf13/viper"
	"log"
)

func Get() {
	setDefaultSettings()

	cfgFile := "config"
	viper.SetConfigName(cfgFile)
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		log.Println("(CONFIG) Invalid " + cfgFile + " file: " + err.Error())
	}
}

func setDefaultSettings() {
	viper.SetDefault("settings", map[string]map[string]interface{}{
		"storage": {
			"enabled":  false,
			"provider": "local",
		},
		"cache": {
			"enabled":  false,
			"provider": "memcache",
		},
		"sql": {
			"enabled":  false,
			"provider": "mysql",
		},
		"nosql": {
			"enabled":  false,
			"provider": "cosmos",
		},
		"email": {
			"enabled":  false,
			"provider": "ses",
		},
		"chat": {
			"enabled":  false,
			"provider": "firebase",
		},
	})
}

// DBConfig is config of mySQL database
type MySQLConfig struct {
	Driver     string
	Host       string
	Port       string
	Database   string
	Username   string
	Password   string
	UnixSocket string
	Charset    string
	Collation  string
	Prefix     string
	Strict     bool
	Engine     string
}

// Azure CosmosDB via Mongo API
type CosmosConfig struct {
	Host     string
	Port     int
	Username string
	Password string
	Database string
}

type MemcacheConfig struct {
	Host string
	Port string
}

// RedisConfig is config of redis database
type RedisConfig struct {
	Host     string
	Port     int
	Database int
	Password string
}
