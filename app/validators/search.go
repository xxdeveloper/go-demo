package validators

import (
	"strings"
	"github.com/asaskevich/govalidator"
)

type UrlCredentials struct {
	Url string `valid:"url"`
}

func UrlValidator(data string) bool {

	data = strings.Trim(data, " ")
	// Validation
	return govalidator.IsURL(data)
}