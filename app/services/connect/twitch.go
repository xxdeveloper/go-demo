package connect

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

const (
	baseUrl  = "https://api.twitch.tv/kraken/"
	clientId = "zpb5falsj69ed7sbmn6ebfiywey5eh"
)

var TService TwitchService

func init() {
	client := &http.Client{Timeout: 5 * time.Second}
	TService = TwitchService{client: *client}
}

// TwitchService module
type TwitchService struct {
	client http.Client
}

// TwitchFeed is array of top twitch videos
type TwitchFeed struct {
	TopStreams []TwitchItem `json:"streams"`
}

type TwitchItem struct {
	Name    string        `json:"game"`
	Viewers uint32        `json:"viewers"`
	Height  int16         `json:"video_height"`
	Images  ImagesBlock   `json:"preview"`
	Channel TwitchChannel `json:"channel"`
}

type TwitchChannel struct {
	URL       string `json:"url"`
	Name      string `json:"name"`
	Logo      string `json:"logo"`
	Mature    bool   `json:"mature"`
	Title     string `json:"display_name"`
	Status    string `json:"status"`
	Lang      string `json:"language"`
	Views     int64  `json:"views"`
	Followers int32  `json:"followers"`
}

type ImagesBlock struct {
	Small  string `json:"small"`
	Medium string `json:"medium"`
	Large  string `json:"large"`
}

func (srv *TwitchService) GetTop() *TwitchFeed {

	url := baseUrl + "streams"
	streams := TwitchFeed{}

	if err := srv.getJson(url, &streams); err != nil {
		log.Fatal(err)
	}

	return &streams
}

func (srv *TwitchService) getJson(url string, target interface{}) error {

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}

	req.Header.Set("Accept", "application/vnd.twitchtv.v5+json")
	req.Header.Set("Client-ID", clientId)

	resp, err := srv.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return json.NewDecoder(resp.Body).Decode(target)
}
