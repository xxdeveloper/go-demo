package app

import (
	"demo/app/handlers"
	mdl "demo/app/middleware"
	"demo/app/services/cache"
	"demo/app/services/connect"
	"demo/config"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/xxdeveloper/cors"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

// App entry point
type Root struct {
	Router        *chi.Mux
	Cache         cache.Cache
	OEmbedService *connect.OEmbedService
}

func (r *Root) ConfigInfo() {
	log.Printf("Registered services: %v", config.GetServicesList())
	log.Printf("Requested services: %v", config.GetAvailableServicesList())
}

func (r *Root) Initialize() {
	r.ConfigInfo()
	r.Cache = config.InitService("cache").(cache.Cache)
	r.OEmbedService = config.InitService("oembed").(*connect.OEmbedService)
	r.Router = r.initRouter()
	log.Println("Initialization is successful ...")
}

func (r *Root) Unitialize() {
	//if r.Cache != nil {
	//	r.Cache.Close()
	//}
}

func (r *Root) Run(host string) {
	http.ListenAndServe(host, r.Router)
}

func (r *Root) initRouter() *chi.Mux {

	router := chi.NewRouter()
	initMiddleware(router)

	// Health check
	router.Mount("/check", handlers.CoreRoutes())

	// Frontend routes
	router.Route("/api", func(router chi.Router) {
		// Now: oembed only URLs
		searchHandlers := &handlers.Search{OEmbed: r.OEmbedService}
		router.Mount("/search", searchHandlers.Routes())

		// Channels list
		feedHandlers := &handlers.Feed{Model: r.Cache}
		router.Mount("/feed", feedHandlers.Routes())
	})

	workDir, _ := os.Getwd()
	filesDir := filepath.Join(workDir, "static")
	FileServer(router, "/static", http.Dir(filesDir))

	return router
}

func initMiddleware(router *chi.Mux) {

	mCors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300,
	})

	router.Use(mCors.Handler)
	router.Use(middleware.RequestID)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)
	router.Use(render.SetContentType(render.ContentTypeJSON))
	router.Use(mdl.UserLang("en"))
}

// FileServer conveniently sets up a http.FileServer handler to serve static files from a http.FileSystem.
func FileServer(r chi.Router, path string, root http.FileSystem) {

	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	fs := http.StripPrefix(path, http.FileServer(root))

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))
}
